import React from 'react';
import './user-grouping.scss';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
import { Persona, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';
import { PersonaPresence } from 'office-ui-fabric-react/lib/Persona';
import classNames from 'classnames/bind';
import * as ReactIcons from '@fluentui/react-icons';

let TOTAL_USERS = 55;
let USERS = [];
// let d = new Date();

let presense_list = [
    PersonaPresence.away,
    PersonaPresence.blocked,
    PersonaPresence.busy,
    PersonaPresence.dnd,
    PersonaPresence.none,
    PersonaPresence.offline,
    PersonaPresence.online
];


// function getRandomInt(min, max) {
//     min = Math.ceil(min);
//     max = Math.floor(max);
//     return Math.floor(Math.random() * (max - min + 1)) + min;
// }

for (let i = 0; i < TOTAL_USERS; i++) {
    var randomName = window.faker.name.findName();
    var split = randomName.split(' ');
    var initials = split[0][0] + split[1][0];
    USERS.push({
        "imageUrl": window.faker.internet.avatar(),
        "imageInitials": initials,
        "text": randomName,
        "secondaryText": window.faker.internet.email().toLowerCase(),
        "tertiaryText": "In a meeting",
        "optionalText": "Available at 4:00pm",
        "presence": presense_list[Math.floor(Math.random() * presense_list.length)]
    })
}

let GROUP_USERS = USERS.splice(0, 5);


const overflowProps = { ariaLabel: 'More commands' };
const _items = [
];

const GROUP_NAMES = [
    {
        id: 1,
        name: 'October Members'
    },
    {
        id: 2,
        name: 'Delhi Members'
    },
    {
        id: 3,
        name: 'Female Members'
    },
    {
        id: 4,
        name: 'Above 50(age) Members'
    },
    {
        id: 5,
        name: 'Random Group'
    }
]

export const UserGrouping = (props) => {
    return (
        <div className="app-top-level-screen user-grouping-screen">
            <div className="group-column group-listing">
                <div className="listing-container">
                    <div className="top-bar">
                        <div className="bar-label">
                            <Label>Group List</Label>
                        </div>
                        <div className="menu">
                            <CommandBar
                                items={_items}
                                overflowItems={_overflowItems}
                                overflowButtonProps={overflowProps}
                                farItems={_farItems}
                                ariaLabel="Use left and right arrow keys to navigate between commands"
                            />
                        </div>
                    </div>
                    <div className="search-bar">
                        <SearchBox placeholder="Search Groups" onChange={props.onSearchTextChange} />
                    </div>
                    <div className="user-listing">
                        {GROUP_NAMES.map((group, index) => (
                            <div className={classNames("group-name-item", { "selected": index === 0 })}>
                                {group.name}
                            </div>
                        ))}
                    </div>
                </div>
            </div>
            <div className="group-column group-members">
                <div className="listing-container">
                    <div className="top-bar">
                        <div className="bar-label">
                            <Label>Group Members</Label>
                        </div>
                        <div className="menu">
                            <CommandBar
                                items={_items}
                                overflowItems={_overflowItems}
                                overflowButtonProps={overflowProps}
                                farItems={_farItems}
                                ariaLabel="Use left and right arrow keys to navigate between commands"
                            />
                        </div>
                    </div>
                    <div className="search-bar">
                        <SearchBox placeholder="Search existing members" onChange={props.onSearchTextChange} />
                    </div>

                    <div className="user-listing">
                        {
                            GROUP_USERS.map((user, i) => (
                                <div className={classNames("user-list-item", { "selected": "" === user.imageUrl })} key={i} onClick={() => { }}>
                                    <Persona
                                        size={PersonaSize.size48}
                                        {...user}
                                        hidePersonaDetails={false}
                                        imageAlt="Annie Lindqvist, status is busy"
                                    />
                                    <div className="add-icon">
                                        <ReactIcons.CalculatorMultiplyIcon />
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
            <div className="group-column members-to-add">
                <div className="listing-container">
                    <div className="top-bar">
                        <div className="bar-label">
                            <Label>To Add</Label>
                        </div>
                        <div className="menu">
                            <CommandBar
                                items={_items}
                                overflowItems={_overflowItems}
                                overflowButtonProps={overflowProps}
                                farItems={_farItems}
                                ariaLabel="Use left and right arrow keys to navigate between commands"
                            />
                        </div>
                    </div>
                    <div className="search-bar">
                        <SearchBox placeholder="Search members to add" onChange={props.onSearchTextChange} />
                    </div>

                    <div className="user-listing">
                        {
                            USERS.map((user, i) => (
                                <div className={classNames("user-list-item", { "selected": "" === user.imageUrl })} key={i} onClick={() => { }}>
                                    <Persona
                                        size={PersonaSize.size48}
                                        {...user}
                                        hidePersonaDetails={false}
                                        imageAlt="Annie Lindqvist, status is busy"
                                    />
                                    <div className="add-icon">
                                        <ReactIcons.AddIcon></ReactIcons.AddIcon>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </div>
    )
}

const _overflowItems = [
    {
        key: 'newItem',
        text: 'New',
        cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
        iconProps: { iconName: 'Add' },
        onClick: () => { }
    },
    {
        key: 'upload',
        text: 'Upload',
        iconProps: { iconName: 'Upload' },
        // href: 'https://developer.microsoft.com/en-us/fluentui',
    },
    {
        key: 'share',
        text: 'Share',
        iconProps: { iconName: 'Share' },
    },
    {
        key: 'download',
        text: 'Download',
        iconProps: { iconName: 'Download' },
    },
];

const _farItems = [
];
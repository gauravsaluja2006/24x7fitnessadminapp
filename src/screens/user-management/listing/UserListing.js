import React from 'react';
import { Persona, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import './UserListing.scss';

import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
import { SearchBox } from 'office-ui-fabric-react/lib/SearchBox';

import classNames from 'classnames/bind';

const overflowProps = { ariaLabel: 'More commands' };


export const UserListing = (props) => {
    return (
        <div className="user-listing-screen app-top-level-screen">
            <div className="listing-container">
                <div className="top-bar">
                    <div className="menu">
                        <CommandBar
                            items={_items}
                            overflowItems={_overflowItems}
                            overflowButtonProps={overflowProps}
                            farItems={_farItems}
                            ariaLabel="Use left and right arrow keys to navigate between commands"
                        />
                    </div>
                </div>
                <div className="search-bar">
                    <SearchBox placeholder="Search" onChange={props.onSearchTextChange} />
                </div>

                <div className="user-listing">
                    {
                        props.users.map((user, i) => (
                            <div className={classNames("user-list-item", { "selected": props.selectedUser.imageUrl === user.imageUrl })} key={i} onClick={() => props.onUserSelect(user)}>
                                <Persona
                                    size={PersonaSize.size48}
                                    {...user}
                                    hidePersonaDetails={false}
                                    imageAlt="Annie Lindqvist, status is busy"
                                />
                            </div>
                        ))
                    }
                </div>
            </div>
        </div>
    )
}

const _items = [
    {
        key: 'newItem',
        text: 'New',
        cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
        iconProps: { iconName: 'Add' },
    },
    {
        key: 'upload',
        text: 'Upload',
        iconProps: { iconName: 'Upload' },
        // href: 'https://developer.microsoft.com/en-us/fluentui',
    },
    {
        key: 'share',
        text: 'Share',
        iconProps: { iconName: 'Share' },
    },
    {
        key: 'download',
        text: 'Download',
        iconProps: { iconName: 'Download' },
    },
];

const _overflowItems = [
];

const _farItems = [
];

import React, { useState } from 'react';
import './Prescriptions.scss';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
// import { IconButton, IIconProps, IContextualMenuProps, Stack } from 'office-ui-fabric-react';
import * as ReactIcons from '@fluentui/react-icons';

const overflowProps = { ariaLabel: 'More commands' };
// const emojiIcon = { iconName: 'ChevronLeft' };


export const Prescriptions = (props) => {
    const _items = [
        // {
        //     key: 'upload',
        //     text: 'Upload',
        //     iconProps: { iconName: 'Upload' },
        // },
    ];

    const _overflowItems = [
        {
            key: 'upload',
            text: 'Edit',
            iconProps: { iconName: 'Edit' },
            onClick: () => { setEditMode(!editMode); }
        },
    ];
    

    const [editMode, setEditMode] = useState(false);
    return (
        <div className="documents-list-container">
            <div className="top-bar">
                <div className="bar-label">
                    <Label>Conditions/Prescriptions</Label>
                </div>
                <div className="document-menu">
                    <CommandBar
                        items={_items}
                        overflowItems={_overflowItems}
                        overflowButtonProps={overflowProps}
                        farItems={_farItems}
                        ariaLabel="Use left and right arrow keys to navigate between commands"
                    />
                    {/* <IconButton iconProps={emojiIcon} title="Emoji" ariaLabel="Emoji" disabled={false} checked={false} /> */}
                </div>
            </div>
            <div className="document-list">
                {props.prescriptions.map(doc => (
                    <div key={doc.name} className="prescription-item">
                        <div className="doc-name">
                            {doc.name}
                        </div>
                        {editMode && (
                            <div className="doc-icon delete">
                                <ReactIcons.DeleteIcon />
                            </div>
                        )}

                    </div>
                ))}
            </div>
        </div>
    )
}

const _farItems = [
];

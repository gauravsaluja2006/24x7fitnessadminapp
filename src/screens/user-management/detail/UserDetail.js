import React, { useState } from 'react';
import './UserDetail.scss';

import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
import { Persona, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import { AddUserTagDialog } from './dialogs/AddUserTagDialog';
import { AddPrescriptionDialog } from './dialogs/AddPrescriptionDialog';
import { UserCard } from './components/UserCard';
import { Notes } from './components/Notes';
import { DocumentList } from './middle-components/document-list/DocumentList';
import { Prescriptions } from './middle-components/prescriptions/Prescriptions';

const overflowProps = { ariaLabel: 'More commands' };

export const UserDetail = (props) => {
    const [isDialogShown, showDialog] = useState(false);
    const [isPrescriptionDialogShown, showPrescriptionDialog] = useState(false);

    const onDialogClosed = (data) => {
        console.log('#### On Closed: ', data);
        showDialog(false);
    }

    const onPrescriptionDialogClosed = (data) => {
        console.log('#### On Prescription Closed: ', data);
        showPrescriptionDialog(false);
    }
    const _items = [
        {
            key: 'newItem',
            text: 'New',
            cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
            iconProps: { iconName: 'Add' },
            subMenuProps: {
                items: [
                    {
                        key: 'emailMessage',
                        text: 'Medical Report',
                        iconProps: { iconName: 'Mail' },
                        // ['data-automation-id']: 'newEmailButton', // optional
                    },
                    {
                        key: 'calendarEvent',
                        text: 'Prescription',
                        iconProps: { iconName: 'Calendar' },
                    },
                ],
            },
        },
        {
            key: 'upload',
            text: 'Tags',
            iconProps: { iconName: 'Upload' },
            onClick: () => { showDialog(true) }
        },
        {
            key: 'share',
            text: 'Prescription',
            iconProps: { iconName: 'Share' },
            onClick: () => { showPrescriptionDialog(true) }
        },
        {
            key: 'download',
            text: 'Download',
            iconProps: { iconName: 'Download' },
        },
    ];

    return (
        <div className="user-detail">
            <div className="top-bar">
                <div className="user-persona">
                    <Persona
                        size={PersonaSize.size24}
                        {...props.selectedUser}
                        hidePersonaDetails={false}
                        imageAlt="Annie Lindqvist, status is busy"
                    />
                </div>
                <div className="menu">
                    <CommandBar
                        items={_items}
                        overflowItems={_overflowItems}
                        overflowButtonProps={overflowProps}
                        farItems={_farItems}
                        ariaLabel="Use left and right arrow keys to navigate between commands"
                    />
                </div>
            </div>
            <div className="detail-cards">
                <div className="top-row">
                    <UserCard selectedUser={props.selectedUser}></UserCard>
                    <Notes notes_count={props.selectedUser.notes_count} notes={props.selectedUser.notes}></Notes>
                </div>
            </div>
            <div className="break-line"></div>
            <div className="cards-row-two">
                <DocumentList documents={props.selectedUser.documents}></DocumentList>
                <Prescriptions prescriptions={props.selectedUser.prescriptions}></Prescriptions>
            </div>
            <div>

                <AddUserTagDialog username={props.selectedUser.text} onDialogClosed={onDialogClosed} showDialog={isDialogShown}></AddUserTagDialog>
                <AddPrescriptionDialog username={props.selectedUser.text} onDialogClosed={onPrescriptionDialogClosed} showDialog={isPrescriptionDialogShown}></AddPrescriptionDialog>

            </div>
        </div>
    )
}

const _overflowItems = [
    { key: 'move', text: 'Move to...', onClick: () => console.log('Move to'), iconProps: { iconName: 'MoveToFolder' } },
    { key: 'copy', text: 'Copy to...', onClick: () => console.log('Copy to'), iconProps: { iconName: 'Copy' } },
    { key: 'rename', text: 'Rename...', onClick: () => console.log('Rename'), iconProps: { iconName: 'Edit' } },
];

const _farItems = [
];
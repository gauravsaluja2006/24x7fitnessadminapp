import React from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { Checkbox } from 'office-ui-fabric-react/lib/Checkbox';
import "./AddUserTagDialog.scss";

const TAGS = [
  {
    id: 1,
    name: "Blood Pressure"
  },
  {
    id: 2,
    name: "Diabetes"
  },
  {
    id: 3,
    name: "Obesity"
  },
  {
    id: 4,
    name: "Sleep Deprivation"
  },
];

export const AddUserTagDialog = (props) => {
  let checkboxChanged = (data1, data2, data3) => {
    let target = data1.target;
    console.log('Target: ', target.name, data2);
  }
  return (
    <Dialog
      hidden={!props.showDialog}
      onDismiss={props.onDialogClosed}
      dialogContentProps={{
        type: DialogType.normal,
        title: `Tags for ${props.username}`,
        subText: `
          Tags are generally used to mark a specific symptom or condition for a user. You can select multiple values here for each user
        `,
      }}
      modalProps={{
        isBlocking: true,
        styles: { main: { maxWidth: 450 } },
      }}
    >
      <form name="tags-form">
        {
          TAGS.map(tag => (
            <div key={tag.id} className="tag-item">
              <Checkbox name={tag.id} label={tag.name} onChange={checkboxChanged} />
            </div>
          ))
        }
      </form>

      <DialogFooter>
        <PrimaryButton onClick={() => props.onDialogClosed('data')} text="Save" />
        <DefaultButton onClick={() => props.onDialogClosed()} text="Cancel" />
      </DialogFooter>
    </Dialog>
  )
}
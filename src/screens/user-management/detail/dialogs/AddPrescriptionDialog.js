import React from 'react';
import { Dialog, DialogType, DialogFooter } from 'office-ui-fabric-react/lib/Dialog';
import { PrimaryButton, DefaultButton } from 'office-ui-fabric-react/lib/Button';
import { TagPicker } from 'office-ui-fabric-react/lib/Pickers';
// import { mergeStyles } from 'office-ui-fabric-react/lib/Styling';
import "./AddUserTagDialog.scss";

// const rootClass = mergeStyles({
//     maxWidth: 500,
// });

// const checkboxStyles = { root: { margin: '10px 0' } };

const _testTags = [
    'black',
    'blue',
    'brown',
    'cyan',
    'green',
    'magenta',
    'mauve',
    'orange',
    'pink',
    'purple',
    'red',
    'rose',
    'violet',
    'white',
    'yellow',
].map(item => ({ key: item, name: item }));


// const _picker = React.createRef();

// const TAGS = [
//     {
//         id: 1,
//         name: "Blood Pressure"
//     },
//     {
//         id: 2,
//         name: "Diabetes"
//     },
//     {
//         id: 3,
//         name: "Obesity"
//     },
//     {
//         id: 4,
//         name: "Sleep Deprivation"
//     },
// ];

export const AddPrescriptionDialog = (props) => {
    // let checkboxChanged = (data1, data2, data3) => {
    //     let target = data1.target;
    //     console.log('Target: ', target.name, data2);
    // }

    const _getTextFromItem = (item) => {
        return item.name;
    }

    const _onFilterChanged = (filterText, tagList) => {
        let matched = _testTags.filter(tag => tag.name.toLowerCase().indexOf(filterText.toLowerCase()) > -1).filter(tag => !_listContainsDocument(tag, tagList));
        let nmatched = _testTags.filter(tag => tag.name.toLowerCase().indexOf(filterText.toLowerCase()) === -1).filter(tag => !_listContainsDocument(tag, tagList));
        return [...matched, ...nmatched];
    }

    const _listContainsDocument = (tag, tagList) => {
        if (!tagList || !tagList.length || tagList.length === 0) {
            return false;
        }
        return tagList.filter(compareTag => compareTag.key === tag.key).length > 0;
    }

    return (
        <Dialog
            hidden={!props.showDialog}
            onDismiss={props.onDialogClosed}
            dialogContentProps={{
                type: DialogType.normal,
                title: `Prescription for ${props.username}`,
                subText: ``,
            }}
            modalProps={{
                isBlocking: true,
                styles: { main: { maxWidth:750 } },
            }}
        >
            <TagPicker
                onClick={() => { }}
                removeButtonAriaLabel="Remove"
                onResolveSuggestions={_onFilterChanged}
                getTextFromItem={_getTextFromItem}
                pickerSuggestionsProps={{
                    suggestionsHeaderText: 'Suggested Tags',
                    noResultsFoundText: 'No Color Tags Found',
                }}
                itemLimit={5}
                disabled={false}
                inputProps={{
                    onBlur: (ev) => console.log('onBlur called'),
                    onFocus: (ev) => console.log('onFocus called'),
                    'aria-label': 'Tag Picker',
                }}
            />

            <DialogFooter>
                <PrimaryButton onClick={() => props.onDialogClosed('data')} text="Save" />
                <DefaultButton onClick={() => props.onDialogClosed()} text="Cancel" />
            </DialogFooter>
        </Dialog>
    )
}
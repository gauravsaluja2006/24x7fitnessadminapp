import React from 'react';
import './UserCard.scss';

import { Persona, PersonaSize } from 'office-ui-fabric-react/lib/Persona';
import { DefaultButton } from 'office-ui-fabric-react';

import { Label } from 'office-ui-fabric-react/lib/Label';

const menuProps = {
    items: [
        {
            key: 'emailMessage',
            text: 'Pre-Defined Message',
            iconProps: { iconName: 'Message' },
        },
    ],
};

export const UserCard = (props) => {
    return (
        <div className="user-card-container">
            <div className="user-card-left">
                <Persona
                    size={PersonaSize.size72}
                    {...props.selectedUser}
                    hidePersonaDetails={true}
                    imageAlt="Annie Lindqvist, status is busy"
                />

                <Label className="user-name">{props.selectedUser.text}</Label>
                <Label className="second-label" disabled={true}>{props.selectedUser.secondaryText}</Label>
                <div className="user-stats">
                    <div className="user-stats-item border stats-left">
                        <Label className="user-name">{props.selectedUser.fasts}</Label>
                        <Label className="second-label" disabled={true}>Fasts</Label>
                    </div>
                    <div className="user-stats-item stats-left">
                        <Label className="user-name">{props.selectedUser.workouts}</Label>
                        <Label className="second-label" disabled={true}>Workouts</Label>
                    </div>
                </div>
                <br />
                <div>
                    <DefaultButton
                        text="Message"
                        split
                        menuProps={menuProps}
                        onClick={() => { }}
                        disabled={false}
                        checked={false}
                    />
                </div>
            </div>
            <div className="user-card-right">
                <div className="right-stats-container">
                    <div>
                        <Label className="stat-label">{props.selectedUser.phone}</Label>
                        <Label className="stat-second-label" disabled={true}>Phone Number</Label>
                    </div>
                    <div>
                        <Label className="stat-label">{props.selectedUser.city}</Label>
                        <Label className="stat-second-label" disabled={true}>City</Label>
                    </div>
                    <div>
                        <Label className="stat-label">ACTIVE</Label>
                        <Label className="stat-second-label" disabled={true}>Membership Status</Label>
                    </div>
                    <div>
                        <Label className="stat-label">{props.selectedUser.member_since.toString()}</Label>
                        <Label className="stat-second-label" disabled={true}>Member Since</Label>
                    </div>
                </div>
            </div>
        </div>
    )
}
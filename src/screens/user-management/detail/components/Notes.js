import React, { useState } from 'react';
import './Notes.scss';
import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';
import { Label } from 'office-ui-fabric-react/lib/Label';
// import { ActivityItem, Link, mergeStyleSets } from 'office-ui-fabric-react';
import classNames from 'classnames/bind';
import { IconButton } from 'office-ui-fabric-react';

const overflowProps = { ariaLabel: 'More commands' };

// const classNames = mergeStyleSets({
//     exampleRoot: {
//         marginTop: '20px',
//     },
//     nameText: {
//         fontWeight: 'bold',
//     },
// });

const emojiIcon = { iconName: 'ChevronLeft' };
const emojiIcon2 = { iconName: 'ChevronRight' };


const _items = [
    {
        key: 'newItem',
        text: 'New Note',
        cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
        iconProps: { iconName: 'Add' },
        onClick: () => { }
    },
];

export const Notes = (props) => {
    const [note, setNote] = useState(props.notes);

    const handleChange = (event) => {
        setNote(event.target.value);
    };
    return (
        <div className="notes-card">
            <div className="top-bar">
                <div className="bar-label">
                    <Label>Notes</Label>
                </div>
                <div className="notes-menu">
                    <div>
                        <IconButton iconProps={emojiIcon} title="Emoji" ariaLabel="Emoji" disabled={false} checked={false} />
                    </div>
                    <div className="notes-paginator">
                        1 | {props.notes_count}
                    </div>
                    <div>
                        <IconButton iconProps={emojiIcon2} title="Emoji" ariaLabel="Emoji" disabled={false} checked={false} />
                    </div>
                    <div className="commander-menu">
                        <CommandBar
                            items={_items}
                            overflowItems={_overflowItems}
                            overflowButtonProps={overflowProps}
                            farItems={_farItems}
                            ariaLabel="Use left and right arrow keys to navigate between commands"
                        />
                    </div>
                </div>
            </div>
            <div className="textarea-container">
                <textarea id="note-taker" value={note} onChange={handleChange} className={classNames({ "is-empty": !note })} />
            </div>
        </div>
    )
}

const _overflowItems = [
];

const _farItems = [
];

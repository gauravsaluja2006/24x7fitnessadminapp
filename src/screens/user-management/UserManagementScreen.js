import React, { useState } from 'react';
import { PersonaPresence } from 'office-ui-fabric-react/lib/Persona';
import './UserManagementScreen.scss';
import { UserListing } from './listing/UserListing';
import { UserDetail } from './detail/UserDetail';
import { UserFeed } from './user-feed/UserFeed';

const moment = require('moment');

let presense_list = [
    PersonaPresence.away,
    PersonaPresence.blocked,
    PersonaPresence.busy,
    PersonaPresence.dnd,
    PersonaPresence.none,
    PersonaPresence.offline,
    PersonaPresence.online
];

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

const documents = [
    {
        name: 'Check up Result.pdf'
    },
    {
        name: 'Dental X-Ray Result.pdf'
    },
    {
        name: 'Medical-Prescriptions.pdf'
    },
    {
        name: 'Check up Result 2.pdf'
    },
    {
        name: 'Dental X-Ray Result 2.pdf'
    },
    {
        name: 'Medical-Prescriptions 2.pdf'
    },
    {
        name: 'Check up Result 3.pdf'
    },
    {
        name: 'Dental X-Ray Result 3.pdf'
    },
    {
        name: 'Medical-Prescriptions 3.pdf'
    },
    {
        name: 'Check up Result 4.pdf'
    },
    {
        name: 'Dental X-Ray Result 4.pdf'
    },
    {
        name: 'Medical-Prescriptions 4.pdf'
    },
]

const prescriptions = [
    {
        name: 'Insomnia'
    },
    {
        name: 'Diabetes'
    },
    {
        name: 'Workout'
    },
    {
        name: 'Yoga'
    },
    {
        name: 'Intermittent Fasting'
    },
    {
        name: 'Sunbath'
    },
    {
        name: 'Multi-Vitamin'
    },
    {
        name: 'Detox Drink'
    },
    {
        name: 'Signature Fuel'  
    },
]



let TOTAL_USERS = 50;
let USERS = [];
let d = new Date();

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

for (let i = 0; i < TOTAL_USERS; i++) {
    var randomName = window.faker.name.findName();
    var split = randomName.split(' ');
    var initials = split[0][0] + split[1][0];
    USERS.push({
        "imageUrl": window.faker.internet.avatar(),
        "imageInitials": initials,
        "text": randomName,
        "fasts": getRandomInt(10, 100),
        "documents": JSON.parse(JSON.stringify(shuffle(documents))),
        "prescriptions": JSON.parse(JSON.stringify(shuffle(prescriptions))),
        "workouts": getRandomInt(10, 100),
        "notes": window.faker.lorem.sentences(),
        "notes_count": getRandomInt(1, 10),
        "phone": window.faker.phone.phoneNumber().split(" ")[0],
        "city": window.faker.address.city(),
        "member_since": moment(window.faker.date.between(new Date(d.setMonth(d.getMonth() - 3)), new Date())).format('LL'),
        "secondaryText": window.faker.internet.email().toLowerCase(),
        "tertiaryText": "In a meeting",
        "optionalText": "Available at 4:00pm",
        "presence": presense_list[Math.floor(Math.random() * presense_list.length)]
    })
}

export const UserManagementScreen = () => {

    const [userListing, updateUserListing] = useState(USERS);
    const [selectedUser, userSelected] = useState(USERS[0]);

    const onSearchTextChange = (evt, text) => {
        const filteredUsers = USERS.filter(user => user.text.toLowerCase().indexOf(text.toLowerCase()) > -1);
        updateUserListing(filteredUsers);
    }

    return (
        <div className="user-management-screen app-top-level-screen">
            <UserListing onSearchTextChange={onSearchTextChange} selectedUser={selectedUser} onUserSelect={userSelected} users={userListing}></UserListing>
            <UserDetail selectedUser={selectedUser}></UserDetail>
            <UserFeed></UserFeed>
        </div>
    )
}
import React, { useState } from 'react';
import './UserFeed.scss';
import { ActivityItem, Link, mergeStyleSets } from 'office-ui-fabric-react';
import { TestImages } from '@uifabric/example-data';
import { Label } from 'office-ui-fabric-react/lib/Label';
import { CommandBar } from 'office-ui-fabric-react/lib/CommandBar';

const overflowProps = { ariaLabel: 'More commands' };

const classNames = mergeStyleSets({
    exampleRoot: {
        marginTop: '20px',
    },
    nameText: {
        fontWeight: 'bold',
    },
});

let count = 5;

// import classNames from 'classnames/bind';


const activityItemExamples = [
    {
        key: 1,
        activityDescription: [
            <Link
                key={1}
                className={classNames.nameText}
                onClick={() => { }}
            >
                Jack Howden
            </Link>,
            <span key={2}> uploaded </span>,
            <span key={3} className={classNames.nameText}>
                MedicalReport.pdf
        </span>,
        ],
        activityPersonas: [{ imageUrl: TestImages.personaMale }],
        comments: '',
        timeStamp: '23m ago',
    },
    {
        key: 2,
        activityDescription: [
            <Link
                key={1}
                className={classNames.nameText}
                onClick={() => { }}
            >
                Javiera Márquez
            </Link>,
            <span key={2}> and </span>,
            <Link
                key={3}
                className={classNames.nameText}
                onClick={() => { }}
            >
                Amelia Povalіy
        </Link>,
            <span key={4}> completed </span>,
            <Link
                key={5}
                className={classNames.nameText}
                onClick={() => { }}
            >
                16 hours Intermittent Fast
        </Link>,
        ],
        activityPersonas: [{ imageInitials: 'JM', text: 'Javiera Márquez' }, { imageUrl: TestImages.personaFemale }],
        timeStamp: '9:27 am',
    },
    {
        key: 3,
        activityDescription: [
            <Link
                key={1}
                className={classNames.nameText}
                onClick={() => { }}
            >
                Robert Larsson
        </Link>,
            <span key={2}> and </span>,
            <Link
                key={3}
                className={classNames.nameText}
                onClick={() => { }}
            >
                2 others
            </Link>,
            <span key={4}> completed 25 Fasts in total </span>,
        ],
        activityPersonas: [
            { imageInitials: 'RL', text: 'Robert Larsson' },
            { imageUrl: TestImages.personaMale },
            { imageUrl: TestImages.personaFemale },
        ],
        timeStamp: '3 days ago',
    },
    {
        key: 4,
        activityDescription: [
            <Link
                key={1}
                className={classNames.nameText}
                onClick={() => { }}
            >
                Jin Cheng
        </Link>,
            <span key={2}> and </span>,
            <Link
                key={3}
                className={classNames.nameText}
                onClick={() => { }}
            >
                5 others
        </Link>,
            <span key={4}> filled out the initial Signup Form</span>,
        ],
        activityPersonas: [
            { imageInitials: 'JC', text: 'Jin Cheng' },
            { imageUrl: TestImages.personaMale },
            { imageInitials: 'AL', text: 'Annie Lindqvist' },
            { imageUrl: TestImages.personaFemale },
            { imageUrl: TestImages.personaMale },
            { imageUrl: TestImages.personaMale },
        ],
        timeStamp: 'May 1, 2020',
    },
];

let index = 0;
const getActivityItem = () => {
    let newItem = Object.assign({}, activityItemExamples[index++]);
    if (index === 4) { index = 0; }
    newItem.key = count++;
    return newItem;
}

for (let i = 0; i < 30; i++) {
    activityItemExamples.push(getActivityItem());
}


export const UserFeed = (props) => {

    const [FEED_ITEMS, updateFeedItems] = useState(activityItemExamples);

    const addNewFeedItem = () => {
        const updatedItems = [getActivityItem(), ...FEED_ITEMS];
        updateFeedItems(updatedItems);
    }

    const _items = [
        {
            key: 'newItem',
            text: 'New',
            cacheKey: 'myCacheKey', // changing this key will invalidate this item's cache
            iconProps: { iconName: 'Add' },
            onClick: () => { addNewFeedItem(); }
        },
        {
            key: 'upload',
            text: 'Upload',
            iconProps: { iconName: 'Upload' },
            // href: 'https://developer.microsoft.com/en-us/fluentui',
        },
        {
            key: 'share',
            text: 'Share',
            iconProps: { iconName: 'Share' },
        },
        {
            key: 'download',
            text: 'Download',
            iconProps: { iconName: 'Download' },
        },
    ];

    return (
        <div className="user-feed-screen app-top-level-screen">
            <div className="top-bar">
                <div className="bar-label">
                    <Label>Latest Activities</Label>
                </div>
                <div className="menu">
                    <CommandBar
                        items={_items}
                        overflowItems={_overflowItems}
                        overflowButtonProps={overflowProps}
                        farItems={_farItems}
                        ariaLabel="Use left and right arrow keys to navigate between commands"
                    />
                </div>
            </div>
            <div className="user-feed-list">
                {FEED_ITEMS.map((item) => (
                    <ActivityItem {...item} key={item.key} className={classNames.exampleRoot} />
                ))}
            </div>
        </div>
    )
}

const _overflowItems = [
];

const _farItems = [
];

import React, { useState } from 'react';
import './SideMenu.scss';
// import { FontIcon } from 'office-ui-fabric-react/lib/Icon';
// import { mergeStyles } from 'office-ui-fabric-react/lib/Styling';
import * as ReactIcons from '@fluentui/react-icons';
import classNames from 'classnames/bind';
import { Link } from "react-router-dom";
import { useLocation } from 'react-router-dom';
// import {withRouter} from 'react-router-dom';

// const iconClass = mergeStyles({
//     fontSize: 50,
//     height: 50,
//     width: 50,
//     margin: 'auto',
// });

const MENU_ITEMS = [
    {
        name: "1",
        route:"/",
        icon: ReactIcons.AddFriendIcon
    },
    {
        name: "2",
        route:"/groups",
        icon: ReactIcons.AddToShoppingListIcon
    },
    {
        name: "3",
        route:"/1",
        icon: ReactIcons.ArrangeSendBackwardIcon
    },
    {
        name: "4",
        route:"/2",
        icon: ReactIcons.AustralianRulesIcon
    },
    {
        name: "5",
        route:"/3",
        icon: ReactIcons.BufferTimeBeforeIcon
    },
    {
        name: "6",
        route:"/4",
        icon: ReactIcons.BarChart4Icon
    },
]


export const SideMenu = (props) => {
    const [_, selectMenu] = useState(MENU_ITEMS[0].name)
    const location = useLocation();  

    console.log(_);

    return (
        <div className="side-menu">
            <div className="icon-container">
                <img alt="" className="app-icon" src="https://cdn.onlinewebfonts.com/svg/img_357046.png"></img>
            </div>
            <div className="menu-items">
                {MENU_ITEMS.map(menu => (
                    <div key={menu.name} onClick={() => { selectMenu(menu.name) }} className={classNames("menu-item", { "selected": location.pathname === menu.route })}>
                        <Link to={menu.route}>
                            <menu.icon className="menu-icon" />
                        </Link>
                    </div>
                ))}
            </div>
        </div>
    )
}
import React from 'react';
// import logo from './logo.svg';
import './App.css';
import { UserManagementScreen } from './screens/user-management/UserManagementScreen';

import { initializeIcons } from '@uifabric/icons';
// import { UploadedDocuments } from './screens/uploaded-documents/UploadedDocuments';
import { SideMenu } from './screens/side-menu/SideMenu';
// import { createTheme, Customizations } from 'office-ui-fabric-react';

import { Route, BrowserRouter } from 'react-router-dom';
import { UserGrouping } from './screens/user-grouping/user-grouping';

initializeIcons();


// const theme = createTheme({
//   // You can also modify certain other properties such as fontWeight if desired
//   defaultFontStyle: { fontFamily: 'Monaco, Menlo, Consolas' }
// });
// If you're changing font globally, it's best to call this at the root of your app,
// not inside a component lifecycle. To apply locally, use Customizer.
// (loadTheme will also work for applying globally but is no longer recommended.)
// Customizations.applySettings({ theme });

function App() {
  return (
    <div className="App">
      < BrowserRouter >
        <SideMenu></SideMenu>
        <Route exact path="/groups" component={UserGrouping} />
        <Route exact path="/" component={UserManagementScreen} />
      </BrowserRouter>
      {/* <UploadedDocuments></UploadedDocuments> */}
    </div>
  );
}

export default App;
